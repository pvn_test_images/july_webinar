FROM registry.gitlab.com/pvn_test_images/ubi8:main 


RUN set -ex && \
    echo "--BEGIN PRIVATE KEY--" > /private_key && \
    echo "aws_access_key_id=01234567890123456789" > /aws_key && \
    yum autoremove -y && \
    yum clean all && \
    rm -rf /var/cache/yum 

ENTRYPOINT /bin/false

